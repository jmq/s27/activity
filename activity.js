const http = require('http')

//mock items array
let items = [
	{name:"Iphone X", description:"Phone designed and created by Apple", price:30000},
	{name:"Horizon Forbidden West", description:"Newest game for the PS4 and PS5", price:4000},
	{name:"Razer Tiamat", description:"Headset from Razer", price:3000}
]

// First route
http.createServer((req,res) => {

	console.log(req.method);

	//route to GET course array
	if(req.url === '/items' && req.method === 'GET') {
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.end(JSON.stringify(items))
	}

	//route to POST a new user in the items array with details coming from our client.
	if(req.url === '/items' && req.method === 'POST') {
		let requestBody = ""

		req.on('data', data => {console.log(data); requestBody += data})

		req.on('end', () => {
			console.log(requestBody)

			//Convert back to object type
			requestBody = JSON.parse(requestBody)

			items.push(requestBody)
			console.log(items)

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.end(JSON.stringify(items))
		})		
	}	

	// //route to POST and find specific item
	if(req.url === '/items/finditem' && req.method === 'POST') {
		let requestBody = ""

		req.on('data', data => {console.log(data); requestBody += data})

		req.on('end', () => {
			console.log(requestBody)

			//Convert back to object type
			requestBody = JSON.parse(requestBody)
			const findItem = items.find(i => i.name === requestBody.name)

			//items.push(requestBody)
			console.log(findItem)

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.end(JSON.stringify(findItem))
		})		
	}	
}).listen(8000)
console.log(`Server is running on localhost:8000`);